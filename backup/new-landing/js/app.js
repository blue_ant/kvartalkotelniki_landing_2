class Countdown {
    constructor(el) {
        let $el = $(el);
        let $clock = $el.find('.Countdown_clock');

        let until = $clock.data('countdown-until');
        //until = new Date().getTime() + 5000;

        $clock.on('update.countdown', function(event) {

            let tagWrapRegexp = /(\d)/g;
            let replaceWithStr = '<span class="Countdown_digit">$1</span>';

            let daysMarkup = event.strftime("%D").replace(tagWrapRegexp, replaceWithStr);
            let hoursMarkup = event.strftime("%H").replace(tagWrapRegexp, replaceWithStr);
            let minutesMarkup = event.strftime("%M").replace(tagWrapRegexp, replaceWithStr);

            let markup = `
                <div class="Countdown_clockItem">
                    <div class="Countdown_digitsPair">${daysMarkup}</div>
                    <div class="Countdown_itemLabel">ДНИ</div>
                </div>
                <div class="Countdown_clockItem">
                    <div class="Countdown_digitsPair">${hoursMarkup}</div>
                    <div class="Countdown_itemLabel">ЧАСЫ</div>
                </div>
                <div class="Countdown_clockItem">
                    <div class="Countdown_digitsPair">${minutesMarkup}</div>
                    <div class="Countdown_itemLabel">МИНУТЫ</div>
                </div>
            `;
            this.innerHTML = event.strftime(markup);
        }).on('finish.countdown', function(event) {
            event.preventDefault();
            $el.remove();
            console.info("Remove countdown element, because the time is up!");
        }).countdown(until, {
            precision: 60000,
        });
    }
}

new Countdown("#homepageCoundown");

var $window = $(window);

(function($slider) {
    if (!$slider.length) return;

    var init = false;

    function sliderInit() {
        if ($window.width() > 768) {
            if (!init) return;

            $slider.slick('unslick');
        } else {
            if (init) return;

            $slider.slick({
                arrows: false,
                slidesToShow: 3,
                centerMode: true,
                infinite: false,
                variableWidth: true,
                dots: true
            });

            init = true;
        }
    }

    sliderInit();

    $window.on('resize', sliderInit);
})($('[data-slider]'));

(function($about) {
    if (!$about.length) return;

    let curWidth = 1282;
    let $items   = $about.find('.about__item');
    let $inner   = $about.find('.about__inner');

    setIconPosition();

    function setIconPosition() {
        let width = $inner.width();
        let coef  = width / curWidth;

        if (width == curWidth) return;

        curWidth = width;

        $items.each(function() {
            let $this    = $(this);
            let position = $this.position();

            $this.css({
                top: position.top * coef,
                left: position.left * coef
            });
        });
    }

    $window.on('resize', setIconPosition);
})($('[data-about]'))

$(function() {
    $('[data-counter]').each(function() {
        let $counter = $(this);
        let curPos   = $counter.data('counter');
        let $inner   = $counter.find('.header-counter__inner');
        let $info    = $counter.find('.header-counter__info');
        let $percent = $info.find('div:last');

        $inner.css({
            width: curPos + '%'
        });

        $percent.html(curPos + '%');
        $counter.show();

        if ($inner.width() + $info.outerWidth() > $counter.width()) {
            $info.addClass('_right');
        } else if ($inner.width() < $info.outerWidth() / 2) {
            $info.css({
                marginRight: ($inner.width() - $info.outerWidth() / 2) - $inner.width()
            }).addClass('_left');
        }
    });
});