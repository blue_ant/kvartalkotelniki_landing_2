(function($calc) {
    if (!$calc.length) return;

    let minRate  = $calc.data('min-bank-rate');
    let minPrice = $calc.data('min-price');
    let fee;
    let term;

    $('[data-range]').each(function() {
        var $slider  = $(this);
        var type     = $slider.data('range');
        var $wrapper = $slider.closest('.MortgageSliders_option')
        var $value   = $wrapper.find('.MortgageSliders_sum')
        var value    = $slider.attr('value');

        $slider.rangeslider({
            polyfill : false,
            onInit: function() {
                if (type === 'fee') {
                    fee = value;
                    value = parseInt(value).toLocaleString("ru-RU");
                } else if (type === 'term') {
                    term  = value;
                    value = value + amount2Word(value, ['год', 'года', 'лет']);
                }

                $value.html(value);

                calcPayment(fee, term);
            },
            onSlide : function(position, value) {
                if (type === 'fee') {
                    fee   = value;
                    value = parseInt(value).toLocaleString("ru-RU");
                } else if (type === 'term') {
                    term  = value;
                    value = value + amount2Word(value, ['год', 'года', 'лет']);
                }

                $value.html(value);

                calcPayment(fee, term);
            }
        });
    });

    calcPayment(fee, term);

    function calcPayment(fee, term) {
        let c = minPrice - fee;                         // сумма кредита
        let p = minRate;                                  // годовая процентная ставка
        let n = term * 12;                                 // срок кредита в месяцах
        let a = 1 + p / 1200;                                       // знаменатель прогрессии
        let k = Math.pow(a, n) * (a - 1) / (Math.pow(a, n) - 1);    // коэффициент ежемесячного платежа
        let sm = k * c;                                             // ежемесячный платёж

        $('[data-payment]').html(parseInt(sm).toLocaleString("ru-RU"));
    }

    function amount2Word(amount, words) {
        var resOfHundred = amount % 100;
        var restOfTen = amount % 10;
        var resultString;

        switch (true) {
            case (resOfHundred >= 5 && resOfHundred <= 20):
                resultString = ' ' + words[2];

                break;
            default:
                switch (true) {
                    case (restOfTen == 1):
                        resultString = ' ' + words[0];

                        break;
                    case (restOfTen >= 2 && restOfTen <= 4):
                        resultString = ' ' + words[1];

                        break;
                    default:
                        resultString = ' ' + words[2];

                        break;
                }
                break;
        }
        return (resultString);
    }
})($('[data-calc]'));
