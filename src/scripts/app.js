/*=require ./includes/blocks/*.js*/

class Countdown {
		constructor(el) {
				let $el = $(el);
				let $clock = $el.find(".Countdown_clock");

				let until = $clock.data("countdown-until");
				//until = new Date().getTime() + 5000;

				$clock
						.on("update.countdown", function(event) {
								let tagWrapRegexp = /(\d)/g;
								let replaceWithStr = '<span class="Countdown_digit">$1</span>';

								let daysMarkup = event.strftime("%D").replace(tagWrapRegexp, replaceWithStr);
								let hoursMarkup = event.strftime("%H").replace(tagWrapRegexp, replaceWithStr);
								let minutesMarkup = event.strftime("%M").replace(tagWrapRegexp, replaceWithStr);

								let markup = `
								<div class="Countdown_clockItem">
										<div class="Countdown_digitsPair">${daysMarkup}</div>
										<div class="Countdown_itemLabel">ДНИ</div>
								</div>
								<div class="Countdown_clockItem">
										<div class="Countdown_digitsPair">${hoursMarkup}</div>
										<div class="Countdown_itemLabel">ЧАСЫ</div>
								</div>
								<div class="Countdown_clockItem">
										<div class="Countdown_digitsPair">${minutesMarkup}</div>
										<div class="Countdown_itemLabel">МИНУТЫ</div>
								</div>
						`;
								this.innerHTML = event.strftime(markup);
						})
						.on("finish.countdown", function(event) {
								event.preventDefault();
								$el.remove();
								console.info("Remove countdown element, because the time is up!");
						})
						.countdown(until, {
								precision: 60000,
						});
		}
}

new Countdown("#homepageCoundown");

var $window = $(window);

(function($slider) {
		if (!$slider.length) return;

		var init = false;

		function sliderInit() {
				if ($window.width() > 768) {
						if (!init) return;

						$slider.slick("unslick");
				} else {
						if (init) return;

						$slider.slick({
								arrows: false,
								slidesToShow: 3,
								centerMode: true,
								infinite: false,
								variableWidth: true,
								dots: true,
						});

						init = true;
				}
		}

		sliderInit();

		$window.on("resize", sliderInit);
})($("[data-slider]"));

(function($about) {
		if (!$about.length) return;

		let curWidth = 1282;
		let $items = $about.find(".about__item");
		let $inner = $about.find(".about__inner");

		setIconPosition();

		function setIconPosition() {
				let width = $inner.width();
				let coef = width / curWidth;

				if (width == curWidth) return;

				curWidth = width;

				$items.each(function() {
						let $this = $(this);
						let position = $this.position();

						$this.css({
								top: position.top * coef,
								left: position.left * coef,
						});
				});
		}

		$window.on("resize", setIconPosition);
})($("[data-about]"));

$(function() {
		$("[data-counter]").each(function() {
				let $counter = $(this);
				let curPos = $counter.data("counter");
				let $inner = $counter.find(".header-counter__inner");
				let $info = $counter.find(".header-counter__info");
				let $percent = $info.find("div:last");

				$inner.css({
						width: curPos + "%",
				});

				$percent.html(curPos + "%");
				$counter.show();

				if ($inner.width() + $info.outerWidth() > $counter.width()) {
						$info.addClass("_right");
				} else if ($inner.width() < $info.outerWidth() / 2) {
						$info
								.css({
										marginRight: $inner.width() - $info.outerWidth() / 2 - $inner.width(),
								})
								.addClass("_left");
				}
		});
});

new Slideshow(".Slideshow");

// Select all links with hashes
$('a[href*="#"]')
		// Remove links that don't actually link to anything
		.not('[href="#"]')
		.not('[href="#0"]')
		.click(function(event) {
				// On-page links
				if (
						location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") &&
						location.hostname == this.hostname
				) {
						// Figure out element to scroll to
						var target = $(this.hash);
						target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
						// Does a scroll target exist?
						if (target.length) {
								// Only prevent default if animation is actually gonna happen
								event.preventDefault();
								$("html, body").animate(
										{
												scrollTop: target.offset().top - 80,
										},
										1000,
										function() {
												// Callback after animation
												// Must change focus!
												var $target = $(target);
												$target.focus();
												if ($target.is(":focus")) {
														// Checking if the target was focused
														return false;
												} else {
														$target.attr("tabindex", "-1"); // Adding tabindex for elements not focusable
														$target.focus(); // Set focus again
												}
										}
								);
						}
				}
		});


// Функция ymaps.ready() будет вызвана, когда
// загрузятся все компоненты API, а также когда будет готово DOM-дерево.
ymaps.ready(init);

console.log(ymaps)

function init() { 
		// Создание карты.    
		var myMap = new ymaps.Map("map", {
				center: [55.656427, 37.828006],
				zoom: 12
		});


		var myPlacemarkWithContent = new ymaps.Placemark([55.638325, 37.857704], {
				hintContent: 'ЖК «Новые Котельники»',
				balloonContent: 'ЖК «Новые Котельники»',
				}, {
						iconLayout: 'default#image',
						iconImageHref: '/new-landing/img/map/logo_mark.png',
						iconImageSize: [48, 48],
						iconImageOffset: [-24, -24],
						iconContentOffset: [15, 15],
				});

	var myPlacemarkWithContent2 = new ymaps.Placemark([55.67372, 37.859038], {
			hintContent: 'метро «Котельники»',
			balloonContent: 'метро «Котельники»',
			}, {
					iconLayout: 'default#image',
					iconImageHref: '/new-landing/img/map/metro.png',
					iconImageSize: [48, 48],
					iconImageOffset: [-24, -24],
					iconContentOffset: [15, 15],
			});

	var myPlacemarkWithContent3 = new ymaps.Placemark([55.632815, 37.766049], {
		hintContent: 'метро «Алма-Атинская»',
		balloonContent: 'метро «Алма-Атинская»',
		}, {
				iconLayout: 'default#image',
				iconImageHref: '/new-landing/img/map/metro.png',
				iconImageSize: [48, 48],
				iconImageOffset: [-24, -24],
				iconContentOffset: [15, 15],
		});

	
	var myPlacemarkWithContent4 = new ymaps.Placemark([55.676491, 37.761831], {
		hintContent: 'метро «Люблино»',
		balloonContent: 'метро «Люблино»',
		}, {
				iconLayout: 'default#image',
				iconImageHref: '/new-landing/img/map/metro.png',
				iconImageSize: [48, 48],
				iconImageOffset: [-24, -24],
				iconContentOffset: [15, 15],
		});

	myMap.geoObjects
		.add(myPlacemarkWithContent)
		.add(myPlacemarkWithContent2)
		.add(myPlacemarkWithContent3)
		.add(myPlacemarkWithContent4)
}