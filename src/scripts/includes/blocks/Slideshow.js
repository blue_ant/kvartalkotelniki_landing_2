class Slideshow {
	constructor(el) {
		let $el = $(el);
		let $pager = $el.find(".Slideshow_pager");
		let $count = $pager.find(".Slideshow_count");
		$el.find(".Slideshow_track")
			.on("beforeChange", function(event, slick, currentSlide, nextSlide) {
				nextSlide++;
				let nextSlideStr = nextSlide + "";
				if (nextSlide < 10) {
					nextSlideStr = "0" + nextSlide;
				}
				$count.html(nextSlideStr);
			})
			.slick({
				// adaptiveHeight: true,
				// prevArrow: `<button type="button" class="Slideshow_arrow Slideshow_arrow-prev">&nbsp;</button>`,
				// nextArrow: `<button type="button" class="Slideshow_arrow Slideshow_arrow-next">&nbsp;</button>`,
				prevArrow: ``,
				nextArrow: ``,
				appendArrows: $el.find(".Slideshow_pager"),
				draggable: false,
			});
	}
}
